from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    file = open('/home/ec2-user/aws-simulation-app/index.html', 'r')
    data = file.read()
    file.close()
    return data

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)