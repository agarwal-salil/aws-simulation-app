#!/bin/bash
# Use this for your user data (script without newlines)
# install python pip -> install git -> install flask -> clone git repository (Linux 2 version)
yum update -y
yum install -y python-pip
yum install -y git
pip install flask
git clone https://agarwal-salil@bitbucket.org/agarwal-salil/aws-simulation-app.git /home/ec2-user/
cp /home/ec2-user/aws-simulation-app/aws-simulation-app.service /lib/systemd/system/aws-simulation-app.service
systemctl daemon-reload
systemctl enable aws-simulation-app.service
systemctl start aws-simulation-app.service